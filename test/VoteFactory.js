const BigNumber = web3.BigNumber;
var ethUtil = require('ethereumjs-util')
var Tx = require('ethereumjs-tx');
const expect = require('chai').expect;
const should = require('chai')
    .use(require('chai-as-promised'))
    .use(require('chai-bignumber')(web3.BigNumber))
    .should();

import expectThrow from './helpers/expectThrow';

var VoteFactory = artifacts.require("./VoteFactory.sol");

contract('VoteFactory', function(accounts) {
    var voteFactory;

    const owner = accounts[0];
    const creator = accounts[1];
    const user0 = accounts[2];
    const user1 = accounts[3];
    const user2 = accounts[4];

    const question0 = "Question 0";
    const newQuestion = "Question New";
    const answer0 = "Answer 0";
    const answer1 = "Answer 1";
    const answer2 = "Answer 2";
    const newAnswer = "Answer New";
    const emptyQuestion = "";
    const emptyAnswer = "";

    beforeEach('setup contract for each test', async function () {
        voteFactory = await VoteFactory.new({from: owner});
    });

    it('created question', async function () {
        await voteFactory.createVote(question0, {from: creator});
        var q0 = await voteFactory.getQuestion(0);
        q0.should.be.equals(question0);
    });

    it('number answers =< 1', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await expectThrow(voteFactory.startVote(0, {from: creator}));
    });

    it('answers added by creator', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        var ans0 =  await voteFactory.getAnswer(0, 0);
        ans0.should.be.equals(answer0);
    });

    it('answer added by user', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await expectThrow(voteFactory.addAnswer(0, answer0, {from: user0}));
    });

    it('missing function parameter', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await expectThrow(voteFactory.addAnswer(answer0, {from: creator}));
    });

    it('set name question by creator', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.setQuestion(0, newQuestion, {from: creator});
        var newQuest = await voteFactory.getQuestion(0);
        newQuest.should.be.equals(newQuestion);
    });

    it('set name answer by creator', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.setAnswer(0, 0, newAnswer, {from: creator});
        var newAns = await voteFactory.getAnswer(0, 0);
        newAns.should.be.equals(newAnswer);
    });
    
    it('set name question by user', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await expectThrow(voteFactory.setQuestion(0, newQuestion, {from: user0}));
    });

    it('set name answer by user', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await expectThrow(voteFactory.setAnswer(0, 0, newAnswer, {from: user0}));
    });

    it('new question equals old', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await expectThrow(voteFactory.setQuestion(0, question0, {from: user0}));
    });

    it('change the name question that does not exist', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await expectThrow(voteFactory.setQuestion(1, {from:creator}));
    });

    it('change the name answer that does not exist', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await expectThrow(voteFactory.setAnswer(0, 2, {from: creator}));
    });
    
    it('new answer equals old', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await expectThrow(voteFactory.setAnswer(0, 0, answer0, {from: creator}));
    });

    it('add answer when vote it started', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await expectThrow(voteFactory.addAnswer(0, answer1, {from: creator}));
    });
    
    it('add answer when vote it stopped', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await voteFactory.stopVote(0, {from: creator});
        await expectThrow(voteFactory.addAnswer(0, answer1, {from: creator}));
    });

    it('check result of vote', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await voteFactory.cast(0, 0, {from: creator});
        await voteFactory.cast(0, 0, {from: user0});
        await voteFactory.cast(0, 1, {from: user1});
        await voteFactory.cast(0, 2, {from: user2});
        await voteFactory.stopVote(0, {from: creator});
        var count = await voteFactory.results(0, {from: creator});
        count.should.be.equals("0");
    });

    it('voted to not exists answer', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await expectThrow(voteFactory.cast(0, 2, {from: user0}));
    });

    it('not unique set vote', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await voteFactory.cast(0, 0, {from: user0});
        await expectThrow(voteFactory.cast(0, 0, {from: user0}));
    });

    it('set vote to another answer', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await voteFactory.cast(0, 0, {from: user0});
        var count = await voteFactory.getCounts(0, 0);
        await count.should.be.equals("1");
        await voteFactory.cast(0, 1, {from: user0})
        count = await voteFactory.getCounts(0, 0);
        await count.should.be.equals("0");
        count = await voteFactory.getCounts(0, 1);
        await count.should.be.equals("1");
    });

    it('get results when vote not stopped', async function () {
        await voteFactory.createVote(question0, {from: creator});
        await voteFactory.addAnswer(0, answer0, {from: creator});
        await voteFactory.addAnswer(0, answer1, {from: creator});
        await voteFactory.startVote(0, {from: creator});
        await voteFactory.cast(0, 0, {from: user0});
        await expectThrow(voteFactory.results(0));
    });
});
