pragma solidity ^0.4.24;

contract BasicVote {
    Vote[] public votes;
    mapping (uint => address) voteToOwner;
    
    struct Vote {
        string question;
        string[] answers;
        State state;
        mapping (address => uint) voteToAnswer;
        mapping (uint => uint) counts;
    }

    enum State {
        Initial,
        Started,
        Stopped
    }

    modifier onlyCreator(uint _voteId) {
        require(voteToOwner[_voteId] == msg.sender);
        _;
    }
    
    modifier checkState(uint _voteId, State _state) {
        require(votes[_voteId].state == _state);
        _;
    }

    function cast(uint _voteId, uint _answerId) external checkState(_voteId, State.Started) {
        require(votes[_voteId].answers.length > _answerId);
        uint oldAnswerId = votes[_voteId].voteToAnswer[msg.sender];
        uint answerId = _answerId + 1;
        require(oldAnswerId != answerId);
        if (oldAnswerId == 0) {
            votes[_voteId].voteToAnswer[msg.sender] = answerId;
            votes[_voteId].counts[answerId]++;
        }
        else {
            votes[_voteId].counts[oldAnswerId]--;
            votes[_voteId].voteToAnswer[msg.sender] = answerId;
            votes[_voteId].counts[answerId]++;
        }
    }
    
    function createVote(string _question) external {
        uint voteId = votes.push(Vote(_question, new string[](0), State.Initial)) - 1;
        voteToOwner[voteId] = msg.sender;
    }
    
    function addAnswer(uint _voteId, string _answer) external onlyCreator(_voteId) checkState(_voteId, State.Initial) {
        votes[_voteId].answers.push(_answer);
    }

    function startVote(uint _voteId) external onlyCreator(_voteId) checkState(_voteId, State.Initial) {
        require(votes[_voteId].answers.length > 1);
        votes[_voteId].state = State.Started;
    }

    function stopVote(uint _voteId) external onlyCreator(_voteId) checkState(_voteId, State.Started) {
        votes[_voteId].state = State.Stopped;
    }
}