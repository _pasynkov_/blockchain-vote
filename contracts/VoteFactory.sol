pragma solidity ^0.4.24;

import "./BasicVote.sol";
import "./Ownable.sol";

contract VoteFactory is BasicVote, Ownable {
    modifier notEqualsString(string _str0, string _str1) {
        require(keccak256(_str0) != keccak256(_str1));
        _;
    }

    function setQuestion(uint _voteId, string _newQuestion) external onlyCreator(_voteId) checkState(_voteId, State.Initial)
    notEqualsString(votes[_voteId].question, _newQuestion) {
        votes[_voteId].question = _newQuestion;
    }

    function setAnswer(uint _voteId, uint _answerId, string _newAnswer) external onlyCreator(_voteId) checkState(_voteId, State.Initial) 
    notEqualsString(votes[_voteId].answers[_answerId], _newAnswer) {
        votes[_voteId].answers[_answerId] = _newAnswer;
    } 

    function getQuestion(uint _voteId) public view returns(string) {
        return votes[_voteId].question;
    }

    function getAnswer(uint _voteId, uint _answerId) public view returns(string) {
        return votes[_voteId].answers[_answerId];
    }

    function getCounts(uint _voteId, uint _answerId) public view returns(string) {
        return uintToStr(votes[_voteId].counts[_answerId + 1]);
    }

    function results(uint _voteId) external view checkState(_voteId, State.Stopped) returns(string) {
        uint winId = 0;
        uint max = 0;
        for (uint i = 0; i < votes[_voteId].answers.length; i++) {
            if(votes[_voteId].counts[i] > max) {
                max = votes[_voteId].counts[i];
                winId = i;
            } 
        }
        return uintToStr(winId - 1);
    }

    function uintToStr(uint i) internal pure returns (string) {
        if (i == 0) return "0";
        bool negative = i < 0;
        uint j = uint(negative ? -i : i);
        uint l = j;     
        uint len;
        while (j != 0){
            len++;
            j /= 10;
        }
        if (negative) ++len;
        bytes memory bstr = new bytes(len);
        uint k = len - 1;
        while (l != 0){
            bstr[k--] = byte(48 + l % 10);
            l /= 10;
        }
        return string(bstr);
    } 
}